﻿using Assignment3EntityFramework.DTO.Character;
using Assignment3EntityFramework.Model;
using Microsoft.EntityFrameworkCore;
using System;

namespace Assignment3EntityFramework.Repositories
{
  public class FranchiseRepository : IFranchiseRepository
  {
    private readonly IMDBContext _context;

    public FranchiseRepository(IMDBContext context)
    {
      _context = context;
    }
    public async Task<bool> Add(Franchise franchise)
    {
      _context.Franchises.Add(franchise);
      try
      {
        await _context.SaveChangesAsync();
      }
      catch
      {
        return false;
      }
      return true;
    }

    public async Task<Task> Delete(Franchise entity)
    {
      _context.Franchises.Remove(entity);
      await _context.SaveChangesAsync();

      return Task.CompletedTask;
    }

    public async Task<IEnumerable<Franchise>> GetAll()
    {
      return await _context.Franchises.ToListAsync();
    }

    public async Task<IEnumerable<Character>> GetAllCharactersInFranchise(int franchiseId)
    {
      var franchise = await _context.Franchises.Where(f => f.Id == franchiseId).Include(f => f.Movies).ThenInclude(c => c.Characters).FirstOrDefaultAsync();
      if (franchise == null || franchise.Movies==null)
      {
        return Enumerable.Empty<Character>();
      }
      Dictionary<int, Character> result = new Dictionary<int, Character>();
      franchise.Movies.ToList().Where(m=>m.Characters!=null).ToList().ForEach(m => m.Characters.ToList().ForEach(c =>
      result.TryAdd(c.Id, c)));
      return result.Values;
    }

    public async Task<IEnumerable<Movie>> GetAllMoviesInFranchise(int franchiseId)
    {
      var franchise = await _context.Franchises.Include(f => f.Movies).Where(f => f.Id == franchiseId).FirstOrDefaultAsync();
      if(franchise == null)
      {
        return Enumerable.Empty<Movie>();
      }
      return franchise.Movies.ToList();
    }

    public async Task<Franchise?> GetById(int id)
    {
      return await _context.Franchises.FindAsync(id);
    }

    public async Task<bool> Update(Franchise entity)
    {
      bool sucuessfull = true;
      try
      {
        _context.Entry(entity).State = EntityState.Modified;
        await _context.SaveChangesAsync();
      }
      catch
      {
        sucuessfull = false;
      }
      return sucuessfull;
    }
    
  }
}
