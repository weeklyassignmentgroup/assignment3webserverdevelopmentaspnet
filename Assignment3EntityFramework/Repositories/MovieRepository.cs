﻿using Assignment3EntityFramework.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Assignment3EntityFramework.Repositories
{
  public class MovieRepository : IMovieRepository
  {
    private readonly IMDBContext _context;


    public MovieRepository(IMDBContext context)
    {
      _context = context;
    }
    public async Task<bool> Add(Movie entity)
    {
      _context.Movies.Add(entity);
      try
      {
        await _context.SaveChangesAsync();
      }
      catch
      {
        return false;
      }
      return true;
    }

    public async Task<Task> Delete(Movie entity)
    {
      _context.Movies.Remove(entity);
      await _context.SaveChangesAsync();
      return Task.CompletedTask;
    }

    public async Task<IEnumerable<Movie>> GetAll()
    {
      return await _context.Movies.ToListAsync();
    }

    public async Task<Movie?> GetById(int id)
    {
      return await _context.Movies.FindAsync(id);
    }

    public async Task<bool> PutManyMovies(int franchiseId, int[] movieIds)
    {
      var franchise = _context.Franchises.Where(f => f.Id == franchiseId).Include(m => m.Movies).FirstOrDefault();

      if (franchise == null) return false;
      if (franchise.Movies == null) franchise.Movies = new List<Movie>();

      franchise.Movies.Clear();

      //Get a list of characters from the int ids
      foreach (int mov in movieIds)
      {
        Movie? movie = _context.Movies.Where(c => c.Id == mov).FirstOrDefault();


        if (movie == null) return false;

        franchise.Movies.Add(movie);

      }
      try
      {
        _context.Entry(franchise).State = EntityState.Modified;
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return false;
      }
      return true;
    }

    public async Task<bool> Update(Movie entity)
    {
      try
      {
        _context.Entry(entity).State = EntityState.Modified;
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return false;
      }
      return true;
    }
  }
}
