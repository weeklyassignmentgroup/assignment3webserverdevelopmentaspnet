﻿namespace Assignment3EntityFramework.Repositories
{
  public interface IMovieRepository: IRepository<Movie>
  {
    public Task<bool> PutManyMovies(int franchiseId, int[] movieIds);

  }
}
