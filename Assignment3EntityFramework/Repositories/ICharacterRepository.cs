﻿namespace Assignment3EntityFramework.Repositories
{
  public interface ICharacterRepository : IRepository<Character>
  {
    public Task<IEnumerable<Character>> GetCharactersInMovie(int movieId);
    public Task<bool> PutCharactersInMovie(int movieId, int[] characters);
 
  }
}
