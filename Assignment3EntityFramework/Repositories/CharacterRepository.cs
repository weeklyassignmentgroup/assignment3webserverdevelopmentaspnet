﻿
using Assignment3EntityFramework.DTO.Character;
using Assignment3EntityFramework.Model;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Assignment3EntityFramework.Repositories
{
  public class CharacterRepository : ICharacterRepository
  {
    private readonly IMDBContext _context;


    public CharacterRepository(IMDBContext context)
    {
      _context = context;
    }

    public async Task<bool> Add(Character entity)
    {
      _context.Characters.Add(entity);

      try
      {
        await _context.SaveChangesAsync();
      }
      catch
      {
        return false;
      }
      return true;
    }

    public async Task<Task> Delete(Character entity)
    {
      _context.Characters.Remove(entity);
      await _context.SaveChangesAsync();
      return Task.CompletedTask;
    }

    public async Task<IEnumerable<Character>> GetAll()
    {
      return await _context.Characters.ToListAsync();
    }

    public async Task<Character?> GetById(int id)
    {
      return await _context.Characters.FindAsync(id);
    }

    public async Task<IEnumerable<Character>> GetCharactersInMovie(int movieId)
    {
      var movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == movieId);

      if(movie == null || movie.Characters == null) return Enumerable.Empty<Character>();

      return movie.Characters.ToList();
    }

    public async Task<bool> PutCharactersInMovie(int movieId, int[] characters)
    {
      var movie = _context.Movies.Where(m => m.Id == movieId).Include(m => m.Characters).FirstOrDefault();
      if (movie == null || movie.Characters == null) return false;

      movie.Characters.Clear();

      foreach (int movieCharaterId in characters)
      {
        Character? charact = _context.Characters.Include(c => c.Movies).Where(c => c.Id == movieCharaterId).FirstOrDefault();
        if (charact == null) continue;

        movie.Characters.Add(charact);
      }
      if (movie.Characters.Count == 0) return false;

      _context.Entry(movie).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        return false;
      }
      return true;
    }

    public async Task<bool> Update(Character entity)
    {
      bool successfull = true;
      try
      {
        _context.Entry(entity).State = EntityState.Modified;
        await _context.SaveChangesAsync();
      }
      catch
      {
        successfull = false;
      }
      return successfull;
    }
  }
}
