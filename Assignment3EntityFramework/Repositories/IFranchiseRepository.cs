﻿using Assignment3EntityFramework.Model;

namespace Assignment3EntityFramework.Repositories
{
  public interface IFranchiseRepository : IRepository<Franchise>
  {
    public Task<IEnumerable<Character>> GetAllCharactersInFranchise(int franchiseId);
    public Task<IEnumerable<Movie>> GetAllMoviesInFranchise(int franchiseId);

  }
}
