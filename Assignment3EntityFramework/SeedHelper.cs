﻿using Assignment3EntityFramework.Model;
using System.Security.Cryptography.X509Certificates;

namespace Assignment3EntityFramework;

public class SeedHelper
{

  public static List<Movie> GetMovies()
  {
    List<Movie> movies = new List<Movie>()
    {
      new Movie() { Id = 1, Title = "Avengers", Genre = "Superhero, comedy", ReleaseYear = 2020, Director="Johnny Depp", PictureUrl="https://www.avengers.com/pic", 
        Trailer = "https://www.avengers.com/trailer", FranchiseId=1},
      new Movie() { Id = 2, Title = "Prestige", Genre="Mystery", ReleaseYear=2006, Director="Amber Heard", PictureUrl="https://www.prestige.com/pic", 
        Trailer="https://www.Prestige.com/trailer", FranchiseId=2},
      new Movie() { Id=3, Title = "Hannah Montana the Movie", Genre = "Drama, musical", ReleaseYear = 2009, Director="Harry Styles", PictureUrl = "https://www.hmtm.com/pic", 
        Trailer = "https://www.hmtm.com/trailer", FranchiseId = 3},
    };

    return movies;
  }

  public static List<Franchise> GetFranchises()
  {
    List<Franchise> franchises = new List<Franchise>()
    {
      new Franchise(){Id = 1, Description="This is universe with superheroes that fights something.", Name="Marvel"},
      new Franchise(){Id=2, Description="This is a magician mystery", Name="Tesla"},
      new Franchise(){Id=3, Description="This is drama and music", Name="Disney"}
    };
    return franchises;
  }

  public static List<Character> GetCharacters()
  {
   

    List<Character> characters = new List<Character>()
    {
      new Character(){Id=1, Alias="Hulken", FullName= "Brannor", Gender="Male", PictureUrl="https:///www.hulk.com/pic" },
      new Character(){Id=2, FullName="Robert Angier", Gender="Male", PictureUrl="https:///www.angier.com/pic"},
      new Character(){Id=3, Alias="Hannah Montana", FullName="Miley Cyrus", Gender="Female", PictureUrl="https:///www.hm.com/pic"},
      new Character(){Id=4, Alias="Thor", FullName="Thor med hammern", Gender="Male", PictureUrl="https:///www.thor.com/pic"}
    };
    return characters;
  }
    /*
  public static List<CharacterMovie> GetCharacterMovies()
  {
    List<CharacterMovie> characterMovie = new List<CharacterMovie>()
    {
      new CharacterMovie(){MovieId=1, CharacterId=1},
      new CharacterMovie(){MovieId=1, CharacterId=4},
      new CharacterMovie(){MovieId=2, CharacterId=2},
      new CharacterMovie(){MovieId=3, CharacterId=3},
    };
    
    return characterMovie;
  }
    */
    

}
