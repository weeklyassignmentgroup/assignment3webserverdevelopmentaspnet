﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3EntityFramework.Model;

public class Franchise
{

  public int Id { get; set; }
  [MaxLength(200)]
  [Required]
  public string Name { get; set; }

  [MaxLength(2000)]
  public string Description { get; set; }

  //Navigation properties

  public ICollection<Movie>? Movies { get; set; }

}
