﻿using Assignment3EntityFramework.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class Movie
{

    public int Id { get; set; }

    [MaxLength(200)]
    [Required]
    public string Title { get; set; }

    [MaxLength(1000)]
    public string Genre { get; set; }


    public int ReleaseYear { get; set; }

    [MaxLength(200)]
    public string Director { get; set; }

    [MaxLength(200)]
    public string? PictureUrl { get; set; }

    [MaxLength(200)]
    public string? Trailer { get; set; }
    
  //NAV
    public ICollection<Character>? Characters { get; set; }
    public Franchise? Franchise { get; set; }
    [ForeignKey("FranchiseFK")]
    public int? FranchiseId { get; set; }



    public Movie()
    {
    }


}
