﻿using System;
using System.ComponentModel.DataAnnotations;

public class Character
{
    public int Id { get; set; }
    [MaxLength(100)]
    [Required]
    public string FullName { get; set; }

    [MaxLength(100)]
    public string? Alias { get; set; }

    [MaxLength(100)]
    public string Gender { get; set; }

    [MaxLength(100)]
    public string? PictureUrl { get; set; }

    //NavPropererties
    public ICollection<Movie>? Movies { get; set; }
    public Character()
    {

    }
}
   