﻿using Microsoft.EntityFrameworkCore;

namespace Assignment3EntityFramework.Model;

public class IMDBContext : DbContext
{
    public DbSet<Movie> Movies { get; set; }
    public DbSet<Character> Characters { get; set; }
    public DbSet<Franchise> Franchises { get; set; }


    public IMDBContext(DbContextOptions options) : base(options)
    {
    }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        //modelBuilder.Entity<CharacterMovie>().HasKey(x => new { x.MovieId, x.CharacterId });

        modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacters());
        modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchises());
        modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovies());

        modelBuilder.Entity<Franchise>().HasMany<Movie>(f => f.Movies).WithOne(m => m.Franchise).OnDelete(DeleteBehavior.SetNull);

      modelBuilder.Entity<Movie>().
            HasMany(m => m.Characters).
            WithMany(c => c.Movies).
            UsingEntity<Dictionary<string, object>>
            ("CharacterMovie",
            r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
            l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
            je =>
            {
                je.HasKey("MovieId", "CharacterId");
                je.HasData(
                    new { MovieId = 1, CharacterId = 1 },
                    new { MovieId = 1, CharacterId = 4 },
                    new { MovieId = 2, CharacterId = 2 },
                    new { MovieId = 3, CharacterId = 3 });
            });

    }
}
