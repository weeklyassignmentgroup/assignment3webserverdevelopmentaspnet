﻿using Assignment3EntityFramework.DTO.Character;
using AutoMapper;

namespace Assignment3EntityFramework.Profiles;

public class CharacterProfile : Profile
{
  public CharacterProfile()
  {
    CreateMap<Character, CharacterReadDTO>();
    CreateMap<CharacterUpdateDTO, Character>();
    CreateMap<CharacterCreateDTO, Character>();
  }
}
