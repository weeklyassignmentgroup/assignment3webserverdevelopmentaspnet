﻿using Assignment3EntityFramework.DTO.Franchise;
using Assignment3EntityFramework.Model;
using AutoMapper;

namespace Assignment3EntityFramework.Profiles;

public class FranchiseProfile : Profile
{
  public FranchiseProfile()
  {
    CreateMap<Franchise, FranchiseReadDTO>();
    CreateMap<Franchise, FranchiseUpdateDTO>().ReverseMap();
    CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
   


  }
}
