﻿using Assignment3EntityFramework.DTO.Character;
using Assignment3EntityFramework.DTO.Movie;
using AutoMapper;

namespace Assignment3EntityFramework.Profiles;

public class MovieProfile : Profile
{
  public MovieProfile()
  {

    CreateMap<Movie, MovieReadDTO>().ReverseMap();
    CreateMap<Movie, MovieCreateDTO>().ReverseMap();
    CreateMap<Movie, MovieUpdateDTO>().ReverseMap();

    }
}
