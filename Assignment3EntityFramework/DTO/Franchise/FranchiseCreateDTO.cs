﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3EntityFramework.DTO.Franchise;

public class FranchiseCreateDTO
{

  public string Name { get; set; }

  public string Description { get; set; }


}
