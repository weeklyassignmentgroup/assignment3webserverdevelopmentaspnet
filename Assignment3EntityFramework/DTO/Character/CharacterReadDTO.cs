﻿using System.ComponentModel.DataAnnotations;

namespace Assignment3EntityFramework.DTO.Character;

public class CharacterReadDTO
{
    public int Id { get; set; }
    public string FullName { get; set; }

    public string? Alias { get; set; }

    public string Gender { get; set; }

    public string? PictureUrl { get; set; }


}
