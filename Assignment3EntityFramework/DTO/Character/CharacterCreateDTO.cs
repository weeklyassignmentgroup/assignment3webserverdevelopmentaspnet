﻿namespace Assignment3EntityFramework.DTO.Character
{
  public class CharacterCreateDTO
  {
    public string FullName { get; set; }

    public string? Alias { get; set; }

    public string Gender { get; set; }

    public string? PictureUrl { get; set; }
  }
}
