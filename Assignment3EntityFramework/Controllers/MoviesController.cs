﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3EntityFramework.Model;
using System.Net.Mime;
using AutoMapper;
using Assignment3EntityFramework.DTO.Movie;
using Assignment3EntityFramework.Repositories;

namespace Assignment3EntityFramework.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [Produces(MediaTypeNames.Application.Json)]
  [Consumes(MediaTypeNames.Application.Json)]
  [ApiConventionType(typeof(DefaultApiConventions))]
  public class MoviesController : ControllerBase
  {
    private readonly IMapper _mapper;
    private readonly IMovieRepository _repo;

    public MoviesController(IMapper mapper, IMovieRepository repo)
    {
      _mapper = mapper;
      _repo = repo;
    }

    // GET: api/Movies
    /// <summary>
    /// Get all movies in database
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
    {
      return _mapper.Map<List<MovieReadDTO>>(await _repo.GetAll());
    }

    // GET: api/Movies/5
    /// <summary>
    /// Get single movie by movieID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
    {
      var movie = _mapper.Map<MovieReadDTO>(await _repo.GetById(id));
      if (movie == null) return NotFound();
      return movie;
    }



    // PUT: api/Movies/5
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    /// <summary>
    /// Update movie by MovieID
    /// </summary>
    /// <param name="movie"></param>
    /// <returns></returns>
    [HttpPut()]
    public async Task<IActionResult> PutMovie(MovieUpdateDTO movie)
    {
      Movie domainMovie = _mapper.Map<Movie>(movie);
      bool sucuess = await _repo.Update(domainMovie);
      if (!sucuess) return BadRequest();

      return NoContent();
    }

    /// <summary>
    /// Update movies in franchise by franchiseID. Set movieID in body and they will all be mapped to the franchiseID in header
    /// </summary>
    /// <param name="FranchiseId"></param>
    /// <param name="movieIds"></param>
    /// <returns></returns>
    [HttpPut("/api/putmovies/{FranchiseId}")]
    public async Task<IActionResult> PutManyMovies(int FranchiseId, [FromBody] int[] movieIds)
    {
      //AddmoviestoFranchise
      bool sucuess = await _repo.PutManyMovies(FranchiseId, movieIds);
      if(!sucuess) return BadRequest();


      return NoContent();
    }

    // POST: api/Movies
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    /// <summary>
    /// Create new movie instance
    /// </summary>
    /// <param name="movieDTO"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movieDTO)
    {
      Movie movie = _mapper.Map<Movie>(movieDTO);
      await _repo.Add(movie);

      MovieReadDTO newMovie = _mapper.Map<MovieReadDTO>(movie);

      return CreatedAtAction("GetMovie", new { id = newMovie.Id }, newMovie);
    }

    // DELETE: api/Movies/5
    /// <summary>
    /// Delete movie from DB by MovieID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteMovie(int id)
    {

      var movie = await _repo.GetById(id);
      if (movie == null)
      {
        return BadRequest();
      }

      await _repo.Delete(movie);

      return NoContent();
    }


  }
}
