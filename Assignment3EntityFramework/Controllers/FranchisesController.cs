﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3EntityFramework.Model;
using System.Net.Mime;
using AutoMapper;
using Assignment3EntityFramework.DTO;
using Assignment3EntityFramework.DTO.Movie;
using Assignment3EntityFramework.DTO.Franchise;
using Assignment3EntityFramework.DTO.Character;
using Assignment3EntityFramework.Repositories;
using NuGet.DependencyResolver;

namespace Assignment3EntityFramework.Controllers;

[Route("api/[controller]")]
[ApiController]
[Produces(MediaTypeNames.Application.Json)]
[Consumes(MediaTypeNames.Application.Json)]
[ApiConventionType(typeof(DefaultApiConventions))]
public class FranchisesController : ControllerBase
{
  private readonly IMapper _mapper;
  private readonly IFranchiseRepository _repo;
  public FranchisesController(IMapper mapper, IFranchiseRepository repo)
  {
    _mapper = mapper;
    _repo = repo;
  }

  // GET: api/Franchises
  /// <summary>
  /// Gets all the Franchises as a list
  /// </summary>
  /// <returns>List of Franchises</returns>
  [HttpGet]
  public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
  {

    var franchises = await _repo.GetAll();

    return franchises.Select(f => _mapper.Map<Franchise, FranchiseReadDTO>(f)).ToList();
  }

  // GET: api/Franchises/5
  /// <summary>
  /// Gets a specific franchise by it's id 
  /// </summary>
  /// <param name="id"></param>
  /// <returns></returns>
  [HttpGet("{id}")]
  public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
  {
    var franchise = await _repo.GetById(id);

    if (franchise == null)
    {
      return NotFound();
    }

    return _mapper.Map<Franchise, FranchiseReadDTO>(franchise);
  }


  /// <summary>
  /// Gets a list of all the characters in movies that are in a franchise.
  /// </summary>
  /// <param name="franchiseId"></param>
  /// <returns>A list of all the </returns>
  [HttpGet("gettyty/{franchiseId}")]
  public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInFranchise(int franchiseId)
  {
    var result = await _repo.GetAllCharactersInFranchise(franchiseId);
    return result.Select(result => _mapper.Map<CharacterReadDTO>(result)).ToList();
  }


  /// <summary>
  /// Gets a list of movies from a franchise
  /// </summary>
  /// <param name="id">franchise id</param>
  /// <returns>A list of all the movies in a given franchise</returns>
  [HttpGet("getty/{id}")]
  public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMoviesInFranchise(int id)
  {
    //Movie must be dto
    var movies = await _repo.GetAllMoviesInFranchise(id);


    if (movies == null)
    {
      return NotFound();
    }

    return movies.Select(movie => _mapper.Map<Movie, MovieReadDTO>(movie)).ToList();

  }

  // PUT: api/Franchises/5
  /// <summary>
  /// Overwrite a franchise with an updated version
  /// </summary>
  /// <param name="id"></param>
  /// <param name="franchiseUpdateDTO"></param>
  /// <returns></returns>
  [ProducesResponseType(StatusCodes.Status204NoContent)]
  [HttpPut()]
  public async Task<IActionResult> PutFranchise(FranchiseUpdateDTO franchiseUpdateDTO)
  {

    Franchise franchise = _mapper.Map<FranchiseUpdateDTO, Franchise>(franchiseUpdateDTO);

    bool changeOneRow = await _repo.Update(franchise);

    if (!changeOneRow)
    {
      return BadRequest();
    }

    return NoContent();
  }

  // POST: api/Franchises
  /// <summary>
  /// Creates a Franchise in the Database and passes the new object back to the caller with Id.
  /// </summary>
  /// <param name="franchiseCreateDto"></param>
  /// <returns>Statuscode 201 Created, and a FranchiseReadDTO object</returns>
  [HttpPost]
  public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchiseCreateDto)
  {
    Franchise franchise = _mapper.Map<FranchiseCreateDTO, Franchise>(franchiseCreateDto);
    await _repo.Add(franchise);

    FranchiseReadDTO franchiseReadDTO = _mapper.Map<Franchise, FranchiseReadDTO>(franchise);


    return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchiseReadDTO);
  }

  // DELETE: api/Franchises/5
  /// <summary>
  /// Deletes a Franchise from the datbase by Id
  /// </summary>
  /// <param name="id">the id of the franchise</param>
  /// <returns></returns> 
  [HttpDelete("{id}")]
  public async Task<IActionResult> DeleteFranchise(int id)
  {
    var franc = await _repo.GetById(id);
    if(franc == null)
    {
      return BadRequest();
    }
    await _repo.Delete(franc);

    return NoContent();
  }


}
