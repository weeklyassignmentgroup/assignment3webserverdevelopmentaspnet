﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3EntityFramework.Model;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Net.Mime;
using AutoMapper;
using Assignment3EntityFramework.DTO.Character;
using Assignment3EntityFramework.DTO.Franchise;
using Assignment3EntityFramework.Repositories;

namespace Assignment3EntityFramework.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [Produces(MediaTypeNames.Application.Json)]
  [Consumes(MediaTypeNames.Application.Json)]
  [ApiConventionType(typeof(DefaultApiConventions))]
  public class CharactersController : ControllerBase
  {
    private readonly IMapper _mapper;
    private readonly ICharacterRepository _repo;

    public CharactersController(IMapper mapper, ICharacterRepository repository)
    {
      _mapper = mapper;
      _repo = repository;
    }

    // GET: api/Characters
    /// <summary>
    /// Get all characters
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters() 
    {
      return _mapper.Map<List<CharacterReadDTO >> (await _repo.GetAll());
    }

    // GET: api/Characters/5
    /// <summary>
    /// Get character by ID
    /// </summary>
    /// <param name="id">Id of character</param>
    [HttpGet("{id}")]
    public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id) 
    {
      var character = await _repo.GetById(id);

      if (character == null)
      {
        return NotFound();
      }

      return _mapper.Map<Character, CharacterReadDTO>(character); ;
    }

    /// <summary>
    /// Gets all character in movie with MovieID
    /// </summary>
    /// <param name="MovieId">Id of movie</param>
    [HttpGet("getty/{MovieId}")]
    public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInMovie(int MovieId) 
    {
      var movie = await _repo.GetCharactersInMovie(MovieId);

      try { 
        List<CharacterReadDTO> allCharactersInMovie = movie.ToList()
          .Select(character => _mapper.Map<CharacterReadDTO>(character)).ToList();
        return allCharactersInMovie;
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Edits character with ID
    /// </summary>
    /// <param name="id"> Character with ID</param>
    /// <param name="dtoCharacter">Character</param>
    // PUT: api/Characters/5
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPut()]
    public async Task<IActionResult> PutCharacter(CharacterUpdateDTO dtoCharacter) 
    {
      Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
      bool changeOneRow = await _repo.Update(domainCharacter);

      if (!changeOneRow)
      {
        return BadRequest();
      }

      return NoContent();
    }

    /// <summary>
    /// Edits characters in a movie
    /// </summary>
    /// <param name="MovieId">Movie ID</param>
    /// <param name="characters">Array of character ID</param>
    //PUT: api/Characters/5
    [HttpPut("/putty/{MovieId}")] //updating characters in a movie
    public async Task<IActionResult> PutCharactersInMovie(int MovieId, [FromBody] int[] characters) 
    {
      var movie = await _repo.PutCharactersInMovie(MovieId, characters);
      if (!movie) return BadRequest();
      
      return NoContent();
    }

    /// <summary>
    /// Creates a new character
    /// </summary>
    /// <param name="dtoCharacter">New character</param>
    // POST: api/Characters
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPost]
    public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO dtoCharacter) 
    {
      Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
      await _repo.Add(domainCharacter);

      return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, _mapper.Map<CharacterReadDTO>(domainCharacter));
    } 

    /// <summary>
    /// Deletes character
    /// </summary>
    /// <param name="id">Id of character</param>
    // DELETE: api/Characters/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteCharacter(int id)
    {
      
      var character = await _repo.GetById(id);
      if (character == null)
      {
        return BadRequest();
      }

      await _repo.Delete(character);

      return NoContent();
    }

  }
}
