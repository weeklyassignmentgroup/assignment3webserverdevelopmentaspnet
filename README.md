# Assignment 3 - Web Server Development ASP .Net


## Description
An Entity Framework Code First workflow and an ASP.NET Core Web API in C#. 
#### Appendix A
In this appendix, we created a database with information about characters, movies and franchises these movies belongs to. The goal of this appendix was to create a database with entity framework, and seed it with data.

#### Appendix B
The goal of this appendix was to continue on the previously created database and implement API with full CRUD. We created DTOs for the client to interact with model classes and used Swagger/Open API to document the code. To clean up our controllers we used service/repositories.

#### Database diagram
![image.png](./image.png)

## Contributors

- [Øyvind Sande Reitan](https://gitlab.com/hindrance)
- [Synne Sævik](https://gitlab.com/Synnems)
- [Eivind Bertelsen](https://gitlab.com/EivindTB)

## Technologies used
- .Net 6
- Entity Framework
- ASP .Net Core
- MSSQL server

## Packages
- Automapper Extensions (11.0)
- Entity Framework Core Design (6.0.8)
- Entity Framework Core Sql Server (6.0.8)
- Entity Framework Core Tools (6.0.8)
- Swashbuckle.AspNetCore (6.2.3)
